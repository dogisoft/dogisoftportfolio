import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  isLoggedIn: any = false;

  constructor() { }

  getLoggedStatusFlag(){
    return this.isLoggedIn;
  }

  setLoggedStatusFlag(status){
    return this.isLoggedIn = status;
  }
}
