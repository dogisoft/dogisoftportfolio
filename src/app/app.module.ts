import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SharedService } from './shared.service';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CompetenciesComponent } from './pages/competencies/competencies.component';
import { ReferencesComponent } from './pages/references/references.component';
import { TechniquesComponent } from './pages/techniques/techniques.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'techniques', component: TechniquesComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'competencies', component: CompetenciesComponent },
  { path: 'references', component: ReferencesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CompetenciesComponent,
    ReferencesComponent,
    TechniquesComponent,
    ContactComponent,
    NavbarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forRoot(appRoutes, {useHash: true}),
    ModalModule.forRoot()
  ],
  providers: [ SharedService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
